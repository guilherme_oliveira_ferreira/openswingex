package org.openswing.swing.export.java;

import org.openswing.swing.domains.java.Domain;
import org.openswing.swing.domains.java.DomainPair;
import org.openswing.swing.table.columns.client.ComboColumn;

/**
 * <p>Title: OpenSwing Framework</p>
 * <p>Description: Callback method invoked by export routine for each
 * VO value (i.e. in case of Excel files the ConvertValue method is
 * called for each data cell).
 * 
 * Methods can be overrided in order to execute additional tasks.</p>
 * <p>Copyright: Copyright (C) 2013 Bartlomiej Gola</p>
 *
 * <p> This file is part of OpenSwing Framework.
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the (LGPL) Lesser General Public
 * License as published by the Free Software Foundation;
 *
 *                GNU LESSER GENERAL PUBLIC LICENSE
 *                 Version 2.1, February 1999
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *       The author may be contacted at:
 *           gola.bartlomiej@gmail.com</p>
 *
 * @author Bartlomiej Gola
 * @version 1.0
 */
public class GridExportComboColumnCallback implements GridExportColumnCallback {
  
  private final Domain domain;
  
  public GridExportComboColumnCallback(ComboColumn column) {
    domain = column != null ? column.getDomain() : null;
  }

  @Override
  public Object convertValue(Object obj) {
    if (domain != null && obj != null) {
      DomainPair pair = domain.getDomainPair(obj);
      if (pair != null) {
        return pair.getDescription();
      }
    }
    // return original object as the last resort
    return obj;
  }
  
}