set "JAVA_HOME=C:\Program Files\Java\jdk1.7.0_21"
set JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
doskey ant=\apps\apache-ant-1.9.2\bin\ant
@echo.
@echo 	Type "ant" to build the OpenSwing framework.
@echo.
@echo 	Note: JBoss and Apache Tomcat need to be installed and build.properties 
@echo 	file has to be updated (jboss.home and tomcat.home options).
@echo.
@cmd /k
